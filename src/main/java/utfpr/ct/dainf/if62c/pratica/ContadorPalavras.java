/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;


/**
 *
 * @author henrique
 */
public class ContadorPalavras {
    private BufferedReader reader;
    private String caminho;
    
    public ContadorPalavras(String path) throws FileNotFoundException{
        this.caminho = path;
        this.reader = new BufferedReader(new FileReader(path));
    }
   
    public HashMap<String,Integer> getPalavras() throws IOException{
        
        String linha = "";
        String [] frase;
        int valor;
        
        HashMap<String,Integer> palavras = new HashMap<>();
        
        while(reader.ready() ){
            
            String palavra;
            
            linha = reader.readLine();

            frase = linha.split(" ");
                        
            for(int i = 0; i < frase.length; i++){
                valor = 0;
                frase[i] = frase[i].replace(".", "");
                frase[i] = frase[i].replace(",", "");
                if(palavras.containsKey(frase[i]) == true){  
                    valor = palavras.get(frase[i]) + 1;
                    palavras.put(frase[i], valor);
                } else {
                    palavras.put(frase[i], 1);
                }
            }
                
        }
        reader.close();
        return palavras;
    }
    
    /* Grrave em outro arquivo texto a lista de palavras e o 
    número de ocorrências em formato CSV, ou seja, cada linha 
    do arquivo gerado deve conter palavra,número de ocorrências. 
    Antes de gravar o arquivo, ordene as palavras pelo número de ocorrências, 
    em ordem descendente. O arquivo de saída deve ser gerado na mesma pasta
    e ter o nome original mais o sufixo .out, por exemplo, 
    se o arquivo de entrada era /tmp/livro.txt, 
    o arquivo de saída deve ser /tmp/livro.txt.out.*/

    public void setPalavras(HashMap<String,Integer> palavras) throws IOException{
        
        BufferedWriter writer = new BufferedWriter(new FileWriter(caminho + ".out"));
        
        writer.write("PALAVRA,CONTADOR");
        writer.newLine();
        
        Iterator it = palavras.entrySet().iterator();
        
        HashMap.Entry<String, Integer> entrada;
        
        List<HashMap.Entry> lista = new ArrayList<>();
        
        // Mapa para lista
        while(it.hasNext()){
            entrada = (HashMap.Entry)it.next();
            lista.add(entrada);
        }
        
        HashMapEntryComparator comp = new HashMapEntryComparator();
        lista.sort(comp);

        Iterator it2 = lista.iterator();
        
        // escrever no arquivo
        while(it2.hasNext()){
            entrada = (HashMap.Entry)it2.next();
            writer.write(entrada.getKey() + "," + entrada.getValue());
            writer.newLine();
            System.out.println(entrada.getKey() + "," + entrada.getValue());
        }
        
        writer.close();
        
    }
    
}
