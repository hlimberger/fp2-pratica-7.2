/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author henrique
 */

import java.io.IOException;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.ContadorPalavras;
import java.util.HashMap;

public class Pratica72 {
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        String caminho;
        
        System.out.print("Digite o nome do caminho completo para o arquivo: ");
        caminho = sc.next();
        
        try {
            ContadorPalavras contador = new ContadorPalavras(caminho);

            HashMap<String, Integer> mapa = contador.getPalavras();
        
            contador.setPalavras(mapa);
            
        } catch (IOException e){
            System.out.print("AVISO: Arquivo não encontrado.");
        }
        
        
    }
}
