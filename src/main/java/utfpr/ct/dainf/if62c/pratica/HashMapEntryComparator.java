/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author henrique
 * @param <K>
 * @param <V>
 */
public class HashMapEntryComparator <K, V extends Comparable<V>> implements Comparator <HashMap.Entry<K,V>> {
    
    public int compare(HashMap.Entry<K,V> o1, HashMap.Entry<K,V> o2){
        return o2.getValue().compareTo(o1.getValue());
    }
}
